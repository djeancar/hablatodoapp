import mongoose from 'mongoose';

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/TodoApp', { useMongoClient: true });

export default mongoose;
