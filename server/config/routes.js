import { createTodo, getTodos, searchTodos } from '../api/controllers';

export default (app) => {
	app.get('/', (req, res) => res.render('home.html'));

	app.post('/api/todos/create', createTodo);
	app.get('/api/todos/search', searchTodos);
	app.get('/api/todos', getTodos);
}