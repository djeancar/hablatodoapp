import express from 'express';
import template from './config/template';
import staticFiles from './config/static';
import routes from './config/routes';
import bodyParser from './config/bodyParser';

const app = express();

bodyParser(app);
template(app);
staticFiles(app);
routes(app);

app.listen(3000);
