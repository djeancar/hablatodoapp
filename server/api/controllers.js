import Todo from '../models/todo';

export const createTodo = async (req, res) => {
	const { name } = req.body;
	const todo = new Todo({
		name,
	});
	await todo.save();
	return res.json({
		todo,
	});
}

export const getTodos = async (req, res) => {
	const todos = await Todo.find().sort('-createdAt');
	return res.json({
		todos,
	});
};

export const searchTodos = async (req, res) => {
	const { name } = req.query;
	let todos;
	if (!!name) {
		todos = await Todo.find({ name: { $regex: name, $options: 'i' } });
	} else {
		todos = await Todo.find().sort('-createdAt');
	}
	
	return res.json({
		todos,
	});
};
