import {
  GET_TODOS_PENDING,
  GET_TODOS_FULFILLED,
  GET_TODOS_REJECTED,

  CREATE_TODO_PENDING,
  CREATE_TODO_FULFILLED,
  CREATE_TODO_REJECTED,

  SEARCH_TODOS_PENDING,
  SEARCH_TODOS_FULFILLED,
  SEARCH_TODOS_REJECTED,

} from '../actions';

const initialState = {
  todos: [],
  loading: false,
};

const chat = (state = initialState, action) => {
  switch (action.type) {

    case SEARCH_TODOS_FULFILLED: {
      const { todos } = action.payload.body;
      return {
        ...state,
        todos,
      };
      break;
    }

    case CREATE_TODO_PENDING:
      return {
        ...state,
        loading: true,
      };
      break;

    case CREATE_TODO_FULFILLED: {
      const { todo } = action.payload.body;
      return {
        ...state,
        todos: [todo].concat(state.todos),
      };
      break;
    }

    case GET_TODOS_PENDING:
      return {
        ...state,
        loading: true,
      };
      break;

    case GET_TODOS_FULFILLED: {
      const { todos } = action.payload.body;
      return {
        todos,
        loading: false,
      };
      break;
    }

    default:
      return state;
  }
};

export default chat;