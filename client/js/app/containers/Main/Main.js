import React, { Component } from 'react';
import TodoList from '../TodoList/TodoList';
import Search from '../Search/Search';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import styles from './main.scss';
import autobind from 'autobind-decorator';
import {
  createTodo,
  getTodos,
} from '../../actions';

const ENTER_KEY_CODE = 13;

@connect(
  state => ({
  }),
  dispatch => bindActionCreators({
    createTodo,
    getTodos,
  }, dispatch)
)


export default class Main extends Component {

  @autobind
  handleKeyDown(e) {
    if (e.keyCode === ENTER_KEY_CODE) {
      this.props.createTodo({ name: this.refs.text.value });
      this.refs.text.value = '';
    }
  }

  componentWillMount() {
    this.props.getTodos();
  }

  render() {
    return (
      <div class="main">
        <div class="input">
          <input ref="text"
          	class="input"
            type="text"
            placeholder="Add todo"
            onKeyDown={ this.handleKeyDown } />
        </div>
        <TodoList />
        <Search />
      </div>
    );
  }
}