import React, { Component } from 'react';
import Todo from '../Todo/Todo';
import styles from './TodoList.scss';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

@connect(
  state => ({
    todos: state.todo.todos,
  }),
  dispatch => bindActionCreators({

  }, dispatch)
)


export default class TodoList extends Component {

  render() {
    const { todos } = this.props;
    return (
      <div class="todo-list">
        { todos.map(todo => <Todo key={ todo._id } 
                              todo={ todo } />
        )}
      </div>
    );
  }
}