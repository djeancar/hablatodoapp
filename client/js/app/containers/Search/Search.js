import React, { Component } from 'react';
import styles from './search.scss';
import autobind from 'autobind-decorator';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  searchTodos,
} from '../../actions';

@connect(
  state => ({
  }),
  dispatch => bindActionCreators({
    searchTodos,
  }, dispatch)
)

export default class Search extends Component {

  @autobind
  handleKeyUp(e) {
    this.props.searchTodos(this.refs.text.value);
  }

  render() {
    return (
      <div class="search">
        <input
        	ref="text"
        	type="text"
        	placeholder="Search"
          onKeyUp={ this.handleKeyUp } />
      </div>
    );
  }
}
