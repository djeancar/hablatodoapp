import React, { Component } from 'react';
import styles from './header.scss'

const Header = () => {
  return (
    <header class="header">
        <h2>Todo App</h2>
    </header>
  );
};

export default Header;
