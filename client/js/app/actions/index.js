import request from 'superagent';

export const CREATE_TODO_PENDING = 'CREATE_TODO_PENDING';
export const CREATE_TODO_FULFILLED = 'CREATE_TODO_FULFILLED';
export const CREATE_TODO_REJECTED = 'CREATE_USER_REJECTED';

export const GET_TODOS_PENDING = 'GET_TODOS_PENDING';
export const GET_TODOS_FULFILLED = 'GET_TODOS_FULFILLED';
export const GET_TODOS_REJECTED = 'GET_TODOS_REJECTED';

export const SEARCH_TODOS_PENDING = 'SEARCH_TODOS_PENDING';
export const SEARCH_TODOS_FULFILLED = 'SEARCH_TODOS_FULFILLED';
export const SEARCH_TODOS_REJECTED = 'SEARCH_TODOS_REJECTED';

const createTodo = (todo) => {
  return {
    type: 'CREATE_TODO',
    payload: request.post('/api/todos/create').send(todo),
  };
};

const getTodos = () => {
  return {
    type: 'GET_TODOS',
    payload: request.get('/api/todos/'),
  }
};

const searchTodos = (pattern) => {
  return {
    type: 'SEARCH_TODOS',
    payload: request.get('/api/todos/search').query({ name: pattern }),
  }
}

export {
  createTodo,
  getTodos,
  searchTodos,
};
