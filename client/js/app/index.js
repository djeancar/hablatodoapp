import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Layout from './components/Layout/Layout';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import { createLogger } from 'redux-logger';
import thunkMiddleware from 'redux-thunk';
import todoApp from './reducers';
import promise from 'redux-promise-middleware';

const store = createStore(
  todoApp,
  applyMiddleware(
    promise(),
    thunkMiddleware,
    createLogger(),
  )
);

ReactDOM.render(
	<Provider store={ store }>
		<Layout />
	</Provider>,
	document.getElementById('app')
);
