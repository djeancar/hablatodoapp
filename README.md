# My project's Habla Todo App

SELECTORS
=========

I am not using selectos because I think for this project I dont need it, the only components thats rerender more than once are just the components that are using a particular state, the rest just render once

Run the project
===============
1. Clone the repository.
2. cd HablaTodoApp
2. npm i
3. npm start
4. run webpack
5. run mongo server